(defproject embang/xlat "0.1.0-SNAPSHOT"
  :description "Source-to-source translator from m!/Scheme to m!/Clojure"
  :url "http://bitbucket.org/dtolpin/embang-xlat"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [embang "1.0.0-SNAPSHOT"]]
  :main ^:skip-aot embang.xlat
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
