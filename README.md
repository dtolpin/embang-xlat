# embang-xlat

Source-to-source translator from __m!__/Scheme to __m!__/Clojure.

## Usage

    $ lein run < old.clj > new.clj

## License

Copyright © 2015 David Tolpin

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.